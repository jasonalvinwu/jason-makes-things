var express = require("express"),
	app = express(),
	cons = require("consolidate"),
	MongoClient =  require("mongodb").MongoClient,
	Server = require("mongodb").Server;

app.engine("html", cons.swig);
app.set("view engine", "html");
app.set("views", __dirname + "/views");

// Establishes the variables for the MongoDB client and use the "course" database

var mongoclient = new MongoClient(new Server("localhost", 27017));
var db = mongoclient.db("course");

// If encountering a '/', do this:

app.get("/", function(req, res) {

	// Find one document in our collection
	db.collection("hello_combined").findOne({}, function(err, doc) {
		if(err) throw err;
		res.render("hello", doc);
	});
});

// For all other characters encountered other than a '/', do this instead:

app.get("*", function(req, res) {
	res.send("Page Not Found", 404);
});

// Establishes a connection to the Mongo client and listen in on port 8080

mongoclient.open(function(err, mongoclient) {
	if(err) throw err;
	app.listen(8080);
	console.log("Express server started on port 8080");
});
